<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Leaderboard;

class FixMoneyProblem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $collection = Leaderboard::get();

        Schema::table('leaderboards', function (Blueprint $table) {
            $table->dropColumn('money');
        });

        Schema::table('leaderboards', function (Blueprint $table) {
            $table->unsignedBigInteger('money')->after('next_level')->nullable();
        });

        foreach ($collection as $lb) {
            Leaderboard::where('id', $lb->id)->update(['money' => intval($lb->money * 10)]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
