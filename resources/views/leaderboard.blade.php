@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Leaderboard</div>
                @if($leaderboard->isEmpty())
                    <div class="panel-body">
                        <p>There are no records found for the leaderboard</p>
                    </div>
                @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Game-name</th>
                                <th>Money</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leaderboard as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item['user_name'] }}</td>
                                    <td>&euro; {{ number_format(($item['money'] / 10), 2, ",", ".") }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
